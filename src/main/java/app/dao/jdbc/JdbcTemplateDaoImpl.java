package app.dao.jdbc;

import static app.dao.jdbc.JdbcDaoQuery.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import app.domain.Account;
import app.domain.Customer;
import app.domain.HolderType;
import app.domain.Status;

@Repository
public class JdbcTemplateDaoImpl implements JdbcTemplateDao{

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcTemplateDaoImpl(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public Customer findCustomerByExtId(String extId) {

		return jdbcTemplate.queryForObject(FIND_CUSTOMER_BY_EXT_ID, new RowMapper<Customer>() {
			
			@Override
			public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {

				Customer customer = new Customer();
				customer.setId(rs.getInt("id"));
				customer.setExtId(rs.getString("ext_id"));
				customer.setName(rs.getString(rs.getString("name")));
				customer.setName2(rs.getString(rs.getString("name2")));
				customer.setSurname(rs.getString(rs.getString("surname")));
				customer.setSurname2(rs.getString(rs.getString("surname2")));
				customer.setSex(rs.getString("sex").charAt(0));
				customer.setStatus(Status.values()[rs.getInt("status")]);
				return customer;
			}
		}, extId);
	}

	@Override
	public Account findByAccNo(String accNo) {

		return jdbcTemplate.queryForObject(FIND_ACCOUNT_BY_ACCNO, new RowMapper<Account>() {
		
			@Override
			public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
				Account acc = new Account();
				acc.setId(rs.getInt("id"));
				acc.setAccNo(rs.getString("acc_no"));
				acc.setStatus(Status.values()[rs.getInt("status")]);
				acc.setBalance(rs.getBigDecimal("balance"));
				acc.setAvailableBalance(rs.getBigDecimal("available_balance"));
				acc.setOwnFunds(rs.getBigDecimal("own_funds"));
				acc.setTransactionPerDay(rs.getInt("transaction_per_day"));
				acc.setAmountPerDay(rs.getBigDecimal("amount_per_day"));
				acc.setTransactionPerMonth(rs.getInt("transaction_per_month"));
				acc.setAmountPerMonth(rs.getBigDecimal("amount_per_month"));
				return acc;
			}
		}, accNo);
	}

	@Override
	public Integer insertAccount(Account acc) {
		
		return jdbcTemplate.queryForObject(INSERT_ACCOUNT_RETURNING_ID,
				new Object[] { acc.getAccNo(), null, acc.getAmountPerDay(), acc.getAmountPerMonth(),
						acc.getAvailableBalance(), acc.getBalance(), acc.getOwnFunds(), Status.ACTIVE.ordinal(),
						acc.getTransactionPerDay(), acc.getTransactionPerMonth(), acc.getTariff().getExtId() },
				Integer.class);
		
	}

	@Override
	public void updateFuds(Account acc) {
		jdbcTemplate.update(UPDATE_ACCOUNT_FUNDS, acc.getAvailableBalance(), acc.getBalance(), acc.getOwnFunds(),
				acc.getId());
	}

	@Override
	public void insertParticipant(Integer customerId, Integer accountId) {
		insertParticipant(customerId, accountId, Status.ACTIVE);
	}

	@Override
	public void insertParticipant(Integer customerId, Integer accountId, Status status) {
		jdbcTemplate.update(INSERT_PARTICIPANT, status.ordinal(), HolderType.OWNER.ordinal(), customerId,
				accountId);
	}

	@Override
	public void insertCustomer(Customer customer) {
		jdbcTemplate.update(INSERT_CUSTOMER, customer.getExtId(), customer.getName(), customer.getName2(),
				customer.getSurname(), customer.getSurname2(), customer.getSex(), customer.getStatus().ordinal());
	}
	

	@Override
	public void insertCustomers(List<Customer> customers) {
		jdbcTemplate.batchUpdate(INSERT_CUSTOMER, new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Customer customer = customers.get(i);
				ps.setString(1, customer.getExtId());
				ps.setString(2, customer.getName());
				ps.setString(3, customer.getName2());
				ps.setString(4, customer.getSurname());
				ps.setString(5, customer.getSurname2());
				ps.setString(6, String.valueOf(customer.getSex()));
				ps.setInt(7, customer.getStatus().ordinal());
			}
			
			@Override
			public int getBatchSize() {
				return customers.size();
			}
		});
		
	}

	@Override
	public void deleteAll() {
		jdbcTemplate.update(DELETE_ALL_PARTICIPANTS);
		jdbcTemplate.update(DELETE_ALL_CUSTOMERS);
		jdbcTemplate.update(DELETE_ALL_ACCOUNTS);
	}

	@Override
	public Integer getCustomerId(String customerExtId) {
		return jdbcTemplate.queryForObject(GET_CUSTOMER_ID_BY_EXT_ID, Integer.class, customerExtId);
	}


}
