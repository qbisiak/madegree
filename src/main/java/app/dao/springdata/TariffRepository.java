package app.dao.springdata;

import org.springframework.data.repository.CrudRepository;

import app.domain.Tariff;

public interface TariffRepository extends CrudRepository<Tariff, Integer>{
	public Tariff findByExtId(String extId);
}
