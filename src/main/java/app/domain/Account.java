package app.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account implements Serializable {

	private static final long serialVersionUID = 5230370743245468104L;
	private Integer id;
	private Tariff tariff;
	private Account account;
	private String accNo;
	private Status status;
	private BigDecimal balance;
	private BigDecimal availableBalance;
	private BigDecimal ownFunds;
	private Integer transactionPerDay;
	private BigDecimal amountPerDay;
	private Integer transactionPerMonth;
	private BigDecimal amountPerMonth;
	private Set<Participant> participants = new HashSet<>();

	public Account() {
	}

	public Account(Integer id, Status status) {
		this.id = id;
		this.status = status;
	}

	public Account(Integer id, Tariff tariff, Account account, String accNo, Status status, BigDecimal balance,
			BigDecimal availableBalance, BigDecimal ownFunds, Integer transactionPerDay, BigDecimal amountPerDay,
			Integer transactionPerMonth, BigDecimal amountPerMonth, Set<Participant> participants) {
		this.id = id;
		this.tariff = tariff;
		this.account = account;
		this.accNo = accNo;
		this.status = status;
		this.balance = balance;
		this.availableBalance = availableBalance;
		this.ownFunds = ownFunds;
		this.transactionPerDay = transactionPerDay;
		this.amountPerDay = amountPerDay;
		this.transactionPerMonth = transactionPerMonth;
		this.amountPerMonth = amountPerMonth;
		this.participants = participants;
	}

	@Id
	@SequenceGenerator(
			name="account_sequence",
			sequenceName="account_id_seq",
			allocationSize=1)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="account_sequence")
	@Column(
		name="id",
		unique=true,
		nullable=false)
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tariff_id")
	public Tariff getTariff() {
		return this.tariff;
	}

	public void setTariff(Tariff tariff) {
		this.tariff = tariff;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rel_account_id")
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Column(name = "acc_no")
	public String getAccNo() {
		return this.accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Column(name = "balance")
	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	@Column(name = "available_balance")
	public BigDecimal getAvailableBalance() {
		return this.availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	@Column(name = "own_funds")
	public BigDecimal getOwnFunds() {
		return this.ownFunds;
	}

	public void setOwnFunds(BigDecimal ownFunds) {
		this.ownFunds = ownFunds;
	}

	@Column(name = "transaction_per_day")
	public Integer getTransactionPerDay() {
		return this.transactionPerDay;
	}

	public void setTransactionPerDay(Integer transactionPerDay) {
		this.transactionPerDay = transactionPerDay;
	}

	@Column(name = "amount_per_day")
	public BigDecimal getAmountPerDay() {
		return this.amountPerDay;
	}

	public void setAmountPerDay(BigDecimal amountPerDay) {
		this.amountPerDay = amountPerDay;
	}

	@Column(name = "transaction_per_month")
	public Integer getTransactionPerMonth() {
		return this.transactionPerMonth;
	}

	public void setTransactionPerMonth(Integer transactionPerMonth) {
		this.transactionPerMonth = transactionPerMonth;
	}

	@Column(name = "amount_per_month")
	public BigDecimal getAmountPerMonth() {
		return this.amountPerMonth;
	}

	public void setAmountPerMonth(BigDecimal amountPerMonth) {
		this.amountPerMonth = amountPerMonth;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Participant> getParticipants() {
		return this.participants;
	}

	public void setParticipants(Set<Participant> participants) {
		this.participants = participants;
	}

}
