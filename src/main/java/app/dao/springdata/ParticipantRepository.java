package app.dao.springdata;

import org.springframework.data.repository.CrudRepository;

import app.domain.Participant;

public interface ParticipantRepository extends CrudRepository<Participant, Integer>{

}
