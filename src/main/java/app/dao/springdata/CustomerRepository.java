package app.dao.springdata;

import org.springframework.data.repository.CrudRepository;

import app.domain.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer>{
	public Customer findByExtId(String extId);
}
