package app.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.dao.springdata.AccountRepository;
import app.dao.springdata.CustomerRepository;
import app.dao.springdata.ParticipantRepository;
import app.dao.springdata.TariffRepository;
import app.domain.Account;
import app.domain.Customer;
import app.domain.Participant;
import app.domain.Status;
import app.domain.Tariff;

@Service
@Profile("springdata")
public class BankServiceSpringDataImpl implements BankService {

	private AccountRepository accountRepository;
	private ParticipantRepository participantRepository;
	private CustomerRepository customerRepository;
	private TariffRepository tariffRepository;

	@Autowired
	public BankServiceSpringDataImpl(AccountRepository accountRepository, ParticipantRepository participantRepository,
			CustomerRepository customerRepository, TariffRepository tariffRepository) {
		super();
		this.accountRepository = accountRepository;
		this.participantRepository = participantRepository;
		this.customerRepository = customerRepository;
		this.tariffRepository = tariffRepository;
	}

	@Override
	@Transactional
	public void moneyTransfer(String accNo, BigDecimal amount) {
		Account account = accountRepository.findByAccNo(accNo);
		if (account != null) {
			account.setBalance(account.getBalance().add(amount));
			account.setAvailableBalance(account.getAvailableBalance().add(amount));
			accountRepository.save(account);
		}
	}

	@Override
	@Transactional
	public void openAccount(Participant participant) {
		Customer customer = customerRepository.findByExtId(participant.getCustomer().getExtId());
		Tariff tariff = tariffRepository.findByExtId(participant.getAccount().getTariff().getExtId());

		participant.getAccount().setTariff(tariff);
		participant.getAccount().setStatus(Status.ACTIVE);
		participant.setCustomer(customer);

		accountRepository.save(participant.getAccount());
		participantRepository.save(participant);
	}

	@Override
	public void addCustomer(Customer customer) {
		customerRepository.save(customer);
	}
	

	@Override
	public void addCustomers(List<Customer> customers) {
		customerRepository.save(customers);
	}

//	@Override
//	public void deleteAll() {
//		participantRepository.deleteAll();
//		accountRepository.deleteAll();
//		customerRepository.deleteAll();
//	}
	
}
