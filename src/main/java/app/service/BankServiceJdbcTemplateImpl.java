package app.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.dao.jdbc.JdbcTemplateDao;
import app.domain.Account;
import app.domain.Customer;
import app.domain.Participant;

@Service
@Profile("jdbctmpl")
public class BankServiceJdbcTemplateImpl implements BankService {

	private JdbcTemplateDao jdbcDao;
	
	@Autowired
	public BankServiceJdbcTemplateImpl(JdbcTemplateDao jdbcDao) {
		super();
		this.jdbcDao = jdbcDao;
	}

	@Override
	@Transactional
	public void moneyTransfer(String accNo, BigDecimal amount) {
		Account account = jdbcDao.findByAccNo(accNo);
		if (account != null) {
			account.setBalance(account.getBalance().add(amount));
			account.setAvailableBalance(account.getAvailableBalance().add(amount));
			jdbcDao.updateFuds(account);
		}
	}

	@Override
	@Transactional
	public void openAccount(Participant participant) {
		Integer customerId = jdbcDao.getCustomerId(participant.getCustomer().getExtId());
		Integer accountId = jdbcDao.insertAccount(participant.getAccount());
		jdbcDao.insertParticipant(customerId, accountId);
	}

	@Override
	public void addCustomer(Customer customer) {
		jdbcDao.insertCustomer(customer);
	}

	@Override
	public void addCustomers(List<Customer> customers) {
		jdbcDao.insertCustomers(customers);
		
	}

//	@Override
//	public void deleteAll() {
//		jdbcDao.deleteAll();
//	}
}
