package app.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "operation")
public class Operation implements Serializable {

	private static final long serialVersionUID = -5394071047991933842L;
	private Integer id;
	private Participant participant;
	private OperationType operationType;
	private String extId;
	private BigDecimal amount;
	private BigDecimal sumCharge;
	private Date createDate;
	private Date processDate;

	public Operation() {
	}

	public Operation(Integer id, Date createDate) {
		this.id = id;
		this.createDate = createDate;
	}

	public Operation(Integer id, Participant participant, OperationType operationType, String extId, BigDecimal amount,
			BigDecimal sumCharge, Date createDate, Date processDate) {
		this.id = id;
		this.participant = participant;
		this.operationType = operationType;
		this.extId = extId;
		this.amount = amount;
		this.sumCharge = sumCharge;
		this.createDate = createDate;
		this.processDate = processDate;
	}

	@Id
	@SequenceGenerator(name="operation_sequence", sequenceName="operation_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="operation_sequence")
	@Column(name="id", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "participant_id")
	public Participant getParticipant() {
		return this.participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "operation_type_id")
	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	@Column(name = "ext_id")
	public String getExtId() {
		return this.extId;
	}

	public void setExtId(String extId) {
		this.extId = extId;
	}

	@Column(name = "amount")
	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "sum_charge")
	public BigDecimal getSumCharge() {
		return this.sumCharge;
	}

	public void setSumCharge(BigDecimal sumCharge) {
		this.sumCharge = sumCharge;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = false, length = 29)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "process_date", length = 29)
	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

}
