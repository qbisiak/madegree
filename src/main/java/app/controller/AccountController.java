package app.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.domain.Participant;
import app.service.BankService;

@RestController
@RequestMapping("/account")
public class AccountController {
	
	private BankService accountService;

	@Autowired
	public AccountController(BankService accountService) {
		super();
		this.accountService = accountService;
	}

	@RequestMapping("/transfer")
	public @ResponseBody Long transfer(@RequestParam String accNo, @RequestParam BigDecimal amount){
		long startTime = System.nanoTime();
		accountService.moneyTransfer(accNo, amount);
		return System.nanoTime() - startTime;
	}
	
	@RequestMapping("/open")
	public @ResponseBody Long update(@RequestBody Participant participant){
		long startTime = System.nanoTime();
		accountService.openAccount(participant);
		return System.nanoTime() - startTime;
	}
	
}
