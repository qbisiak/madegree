-- Last modification date: 2016-08-14

-- sequences
CREATE SEQUENCE account_id_seq;
CREATE SEQUENCE customer_id_seq;
CREATE SEQUENCE operation_id_seq;
CREATE SEQUENCE operation_type_id_seq;
CREATE SEQUENCE participant_id_seq;
CREATE SEQUENCE product_id_seq;
CREATE SEQUENCE tariff_id_seq;

-- tables
-- Table: Account
CREATE TABLE Account (
    id int  NOT NULL DEFAULT nextval('account_id_seq'),
    acc_no text,
    status int  NOT NULL,
    balance decimal DEFAULT 0,
    available_balance decimal DEFAULT 0,
    own_funds decimal DEFAULT 0,
    transaction_per_day int,
    amount_per_day decimal,
    transaction_per_month int,
    amount_per_month decimal,
    tariff_id int,
    rel_account_id int,
    CONSTRAINT Account_pk PRIMARY KEY (id)
);



-- Table: Customer
CREATE TABLE Customer (
    id int  NOT NULL DEFAULT nextval('customer_id_seq'),
    ext_id text DEFAULT currval('customer_id_seq')::TEXT,
    name text  NOT NULL,
    name2 text,
    surname text  NOT NULL,
    surname2 text,
    sex char(1)  NOT NULL,
    status int  NOT NULL,
    CONSTRAINT Customer_pk PRIMARY KEY (id)
);



-- Table: Operation
CREATE TABLE Operation (
    id int  NOT NULL DEFAULT nextval('operation_id_seq'),
    ext_id text DEFAULT currval('operation_id_seq')::TEXT,
    amount decimal DEFAULT 0,
    sum_charge decimal DEFAULT 0,
    create_date timestamp NOT NULL DEFAULT current_timestamp,
    process_date timestamp,
    participant_id int,
    operation_type_id int,
    CONSTRAINT Operation_pk PRIMARY KEY (id)
);



-- Table: Operation_Type
CREATE TABLE Operation_Type (
    id int  NOT NULL DEFAULT nextval('operation_type_id_seq'),
    ext_id text  DEFAULT currval('operation_type_id_seq')::TEXT,
    const_charge decimal DEFAULT 0,
    interest decimal(3,2) DEFAULT 0,
    tariff_id int,
    CONSTRAINT Operation_Type_pk PRIMARY KEY (id)
);



-- Table: Participant
CREATE TABLE Participant (
    id int  NOT NULL DEFAULT nextval('participant_id_seq'),
    status int  NOT NULL,
    holder_type int NOT NULL,
    customer_id int,
    account_id int,
    CONSTRAINT Participant_pk PRIMARY KEY (id)
);



-- Table: Product
CREATE TABLE Product (
    id int  NOT NULL DEFAULT nextval('product_id_seq'),
    ext_id text  DEFAULT currval('product_id_seq')::TEXT,
    name text  NOT NULL,
    type text,
    CONSTRAINT Product_pk PRIMARY KEY (id)
);



-- Table: Tariff
CREATE TABLE Tariff (
    id int  NOT NULL DEFAULT nextval('tariff_id_seq'),
    ext_id text DEFAULT currval('tariff_id_seq')::TEXT,
    interest decimal(3,2) DEFAULT 0,
    monthly_fee decimal DEFAULT 0,
    annual_fee decimal DEFAULT 0,
    product_id int  NOT NULL,
    CONSTRAINT Tariff_pk PRIMARY KEY (id)
);




-- foreign keys
-- Reference: Account_Account (table: Account)
ALTER TABLE Account ADD CONSTRAINT Account_Account
    FOREIGN KEY (rel_account_id)
    REFERENCES Account (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Account_Tariff (table: Account)
ALTER TABLE Account ADD CONSTRAINT Account_Tariff
    FOREIGN KEY (tariff_id)
    REFERENCES Tariff (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Operation_OperationType (table: Operation)
ALTER TABLE Operation ADD CONSTRAINT Operation_OperationType
    FOREIGN KEY (operation_type_id)
    REFERENCES Operation_Type (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Operation_Participant (table: Operation)
ALTER TABLE Operation ADD CONSTRAINT Operation_Participant
    FOREIGN KEY (participant_id)
    REFERENCES Participant (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Operation_Type_Tariff (table: Operation_Type)
ALTER TABLE Operation_Type ADD CONSTRAINT Operation_Type_Tariff
    FOREIGN KEY (tariff_id)
    REFERENCES Tariff (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Participant_Account (table: Participant)
ALTER TABLE Participant ADD CONSTRAINT Participant_Account
    FOREIGN KEY (account_id)
    REFERENCES Account (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Participant_Customer (table: Participant)
ALTER TABLE Participant ADD CONSTRAINT Participant_Customer
    FOREIGN KEY (customer_id)
    REFERENCES Customer (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Tariff_Product (table: Tariff)
ALTER TABLE Tariff ADD CONSTRAINT Tariff_Product
    FOREIGN KEY (product_id)
    REFERENCES Product (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


--indexes
CREATE INDEX account_idx on account(id);
CREATE INDEX customer_idx on customer(id);
CREATE INDEX operation_idx on operation(id);
CREATE INDEX operation_type_idx on operation_type(id);
CREATE INDEX participant_idx on participant(id);
CREATE INDEX product_idx on product(id);
CREATE INDEX tariff_idx on tariff(id);

CREATE INDEX account_ext_idx on account(acc_no);
CREATE INDEX customer_ext_idx on customer(ext_id);
CREATE INDEX operation_ext_idx on operation(ext_id);
CREATE INDEX operation_type_ext_idx on operation_type(ext_id);
CREATE INDEX product_ext_idx on product(ext_id);
CREATE INDEX tariff_ext_idx on tariff(ext_id);


-- grant
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO magister;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA PUBLIC to magister;





-- End of file.

