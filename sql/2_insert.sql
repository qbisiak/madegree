insert into product (ext_id, name, type) values ('PA', 'personal_account', 'A');
insert into product (ext_id, name, type) values ('DC', 'debit_card', 'C');

insert into tariff (ext_id, interest, monthly_fee, annual_fee, product_id) select 'PA0001', 0, 0, 0, id from product where ext_id = 'PA';
insert into tariff (ext_id, interest, monthly_fee, annual_fee, product_id) select 'PA0002', 0, 5, 0, id from product where ext_id = 'PA';
insert into tariff (ext_id, interest, monthly_fee, annual_fee, product_id) select 'DC0001', 0, 0, 0, id from product where ext_id = 'DC';
insert into tariff (ext_id, interest, monthly_fee, annual_fee, product_id) select 'DC0002', 0, 3, 0, id from product where ext_id = 'DC';

insert into operation_type (ext_id, const_charge, interest, tariff_id) select 'TR_PA0001', 0.25, 0, id from tariff where ext_id = 'PA0001';
insert into operation_type (ext_id, const_charge, interest, tariff_id) select 'TR_PA0002', 0, 0, id from tariff where ext_id = 'PA0002';
insert into operation_type (ext_id, const_charge, interest, tariff_id) select 'TR_DC0001', 0.5, 0, id from tariff where ext_id = 'DC0001';
insert into operation_type (ext_id, const_charge, interest, tariff_id) select 'TR_DC0002', 0, 0, id from tariff where ext_id = 'DC0002';