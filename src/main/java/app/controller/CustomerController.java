package app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.domain.Customer;
import app.service.BankService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	private BankService customerService;

	@Autowired
	public CustomerController(BankService customerService) {
		super();
		this.customerService = customerService;
	}

	@RequestMapping(value = "/add" )
	public @ResponseBody Long add(@RequestBody Customer customer){
		long startTime = System.nanoTime();
		customerService.addCustomer(customer);
		return System.nanoTime() - startTime;
	}
	@RequestMapping(value = "/addList" )
	public @ResponseBody Long addList(@RequestBody List<Customer> customers){
		long startTime = System.nanoTime();
		for (Customer customer : customers) {
			customerService.addCustomer(customer);
		}
		return System.nanoTime() - startTime;
	}
	
	@RequestMapping(value = "/addBatch" )
	public @ResponseBody Long addBatch(@RequestBody List<Customer> customers){
		long startTime = System.nanoTime();
		customerService.addCustomers(customers);
		return System.nanoTime() - startTime;
	}
	
//	@RequestMapping("/clear")
//	public @ResponseBody Long clear(){
//		long startTime = System.nanoTime();
//		customerService.deleteAll();
//		return System.nanoTime() - startTime;
//	}
}
