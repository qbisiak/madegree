ALTER TABLE Account DROP CONSTRAINT Account_Account;
ALTER TABLE Account DROP CONSTRAINT Account_Tariff;
ALTER TABLE Operation DROP CONSTRAINT Operation_OperationType;
ALTER TABLE Operation DROP CONSTRAINT Operation_Participant;
ALTER TABLE Operation_Type DROP CONSTRAINT Operation_Type_Tariff;
ALTER TABLE Participant DROP CONSTRAINT Participant_Account;
ALTER TABLE Participant DROP CONSTRAINT Participant_Customer;
ALTER TABLE Tariff DROP CONSTRAINT Tariff_Product;


delete from participant;
delete from account;
delete from customer;


ALTER TABLE Account ADD CONSTRAINT Account_Account
    FOREIGN KEY (rel_account_id)
    REFERENCES Account (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

ALTER TABLE Account ADD CONSTRAINT Account_Tariff
    FOREIGN KEY (tariff_id)
    REFERENCES Tariff (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


ALTER TABLE Operation ADD CONSTRAINT Operation_OperationType
    FOREIGN KEY (operation_type_id)
    REFERENCES Operation_Type (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


ALTER TABLE Operation ADD CONSTRAINT Operation_Participant
    FOREIGN KEY (participant_id)
    REFERENCES Participant (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


ALTER TABLE Operation_Type ADD CONSTRAINT Operation_Type_Tariff
    FOREIGN KEY (tariff_id)
    REFERENCES Tariff (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


ALTER TABLE Participant ADD CONSTRAINT Participant_Account
    FOREIGN KEY (account_id)
    REFERENCES Account (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


ALTER TABLE Participant ADD CONSTRAINT Participant_Customer
    FOREIGN KEY (customer_id)
    REFERENCES Customer (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;


ALTER TABLE Tariff ADD CONSTRAINT Tariff_Product
    FOREIGN KEY (product_id)
    REFERENCES Product (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

