package app.dao.jdbc;

import static app.dao.jdbc.JdbcDaoQuery.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.domain.Account;
import app.domain.Customer;
import app.domain.HolderType;
import app.domain.Participant;
import app.domain.Status;

@Repository
public class JdbcPreparedStatementDaoImpl implements JdbcPreparedStatementDao {

	private DataSource dataSource;
	
	@Autowired
	public JdbcPreparedStatementDaoImpl(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	@Override
	public void insertCustomer(Customer customer) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(INSERT_CUSTOMER);		
			ps.setString(1, customer.getExtId());
			ps.setString(2, customer.getName());
			ps.setString(3, customer.getName2());
			ps.setString(4, customer.getSurname());
			ps.setString(5, customer.getSurname2());
			ps.setString(6, String.valueOf(customer.getSex()));
			ps.setInt(7, customer.getStatus().ordinal());
			ps.executeUpdate();
			conn.commit();
		
		} catch (SQLException e){
			rollback(conn);
			throw new RuntimeException(e);
		} finally{
			closePreparedStatement(ps);
			closeConnection(conn);
		}
		
	}
	
	
	
	@Override
	public void insertCustomers(List<Customer> customers) {
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			for (Customer customer : customers) {
				ps = conn.prepareStatement(INSERT_CUSTOMER);		
				ps.setString(1, customer.getExtId());
				ps.setString(2, customer.getName());
				ps.setString(3, customer.getName2());
				ps.setString(4, customer.getSurname());
				ps.setString(5, customer.getSurname2());
				ps.setString(6, String.valueOf(customer.getSex()));
				ps.setInt(7, customer.getStatus().ordinal());
				ps.addBatch();
			}
			ps.executeBatch();
			conn.commit();
		
		} catch (SQLException e){
			rollback(conn);
			throw new RuntimeException(e);
		} finally{
			closePreparedStatement(ps);
			closeConnection(conn);
		}
		
	}

	@Override
	public void openAccount(Participant participant) {
		Connection conn = null;
		PreparedStatement psGetCustomer = null;
		PreparedStatement psInsertAccount = null;
		PreparedStatement psInsertParticipant = null;
		try{
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			
			// get customer id
			psGetCustomer = conn.prepareStatement(FIND_CUSTOMER_BY_EXT_ID);
			psGetCustomer.setString(1, participant.getCustomer().getExtId());
			ResultSet rsCustomer = psGetCustomer.executeQuery();
			if (rsCustomer.next()){
				participant.getCustomer().setId(rsCustomer.getInt("id"));
			} else {
				throw new SQLException("Customer not found: " + participant.getCustomer().getExtId());
			}
			
			// insert account returning id
			Account acc = participant.getAccount();
			psInsertAccount = conn.prepareStatement(INSERT_ACCOUNT_RETURNING_ID);
			psInsertAccount.setString(1, acc.getAccNo());
			psInsertAccount.setNull(2, Types.INTEGER);
			psInsertAccount.setBigDecimal(3, acc.getAmountPerDay());
			psInsertAccount.setBigDecimal(4 ,acc.getAmountPerMonth());
			psInsertAccount.setBigDecimal(5, acc.getAvailableBalance());
			psInsertAccount.setBigDecimal(6, acc.getBalance());
			psInsertAccount.setBigDecimal(7, acc.getOwnFunds());
			psInsertAccount.setInt(8, Status.ACTIVE.ordinal());
			psInsertAccount.setInt(9, acc.getTransactionPerDay());
			psInsertAccount.setInt(10, acc.getTransactionPerMonth());
			psInsertAccount.setString(11, acc.getTariff().getExtId()); 
			ResultSet rsAccount = psInsertAccount.executeQuery();
			if (rsAccount.next()){
				acc.setId(rsAccount.getInt(1));
			} else {
				throw new SQLException("Account not found: " + acc.getAccNo());
			}
			
			psInsertParticipant = conn.prepareStatement(INSERT_PARTICIPANT);
			psInsertParticipant.setInt(1, Status.ACTIVE.ordinal());
			psInsertParticipant.setInt(2, HolderType.OWNER.ordinal());
			psInsertParticipant.setInt(3, participant.getCustomer().getId());
			psInsertParticipant.setInt(4, participant.getAccount().getId());
			psInsertParticipant.executeUpdate();
			
			conn.commit();
			
		} catch (SQLException e){
			rollback(conn);
			throw new RuntimeException(e);
		} finally{
			closePreparedStatement(psGetCustomer);
			closePreparedStatement(psInsertAccount);
			closePreparedStatement(psInsertParticipant);
			closeConnection(conn);
		}
	}

	private void closePreparedStatement(PreparedStatement ps) {
		if (ps != null){
			try {
				ps.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void closeConnection(Connection conn) {
		if (conn != null){
			try {
				conn.close();
			} catch (SQLException e){
				throw new RuntimeException(e);
			}
		}
	}

	private void rollback(Connection conn) {
		if (conn != null){
			try {
				conn.rollback();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}


	
	

}
