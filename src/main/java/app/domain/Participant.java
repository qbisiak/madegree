package app.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "participant")
public class Participant implements Serializable {

	private static final long serialVersionUID = 2461884306913366039L;
	private Integer id;
	private Customer customer;
	private Account account;
	private Status status;
	private HolderType holderType;
	private Set<Operation> operations = new HashSet<>();

	public Participant() {
	}

	public Participant(Integer id, Status status) {
		this.id = id;
		this.status = status;
	}
	
	public Participant(Customer customer, Account account){
		this.customer = customer;
		this.account = account;
		this.status = Status.ACTIVE;
		this.holderType = HolderType.OWNER;
	}

	public Participant(Integer id, Customer customer, Account account, Status status, HolderType holderType, Set<Operation> operations) {
		this.id = id;
		this.customer = customer;
		this.account = account;
		this.status = status;
		this.holderType = holderType;
		this.operations = operations;
	}

	@Id
	@SequenceGenerator(name="participant_sequence", sequenceName="participant_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="participant_sequence")
	@Column(name="id", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_id")
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Column(name = "holder_type")
	@Enumerated(EnumType.ORDINAL)
	public HolderType getHolderType() {
		return this.holderType;
	}

	public void setHolderType(HolderType holderType) {
		this.holderType = holderType;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "participant")
	public Set<Operation> getOperations() {
		return this.operations;
	}

	public void setOperations(Set<Operation> operations) {
		this.operations = operations;
	}

}
