package app.dao.jdbc;

import java.util.List;

import app.domain.Account;
import app.domain.Customer;
import app.domain.Status;

public interface JdbcTemplateDao {
	
	public Account findByAccNo(String accNo);
	public void updateFuds(Account acc);
	public void insertParticipant(Integer customerExtId, Integer accountNo);
	public void insertParticipant(Integer customerExtId, Integer accountNo, Status status);
	public Customer findCustomerByExtId(String extId);
	public void insertCustomer(Customer customer);
	public void insertCustomers(List<Customer> customers);
	public Integer insertAccount(Account account);
	public Integer getCustomerId(String customerExtId);
	public void deleteAll();

}
