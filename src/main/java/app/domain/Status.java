package app.domain;

public enum Status { 
	
	OPENNING, ACTIVE, LOCKED, CLOSED;
	
	public String getStatus(){
		return this.name();
	}
}
