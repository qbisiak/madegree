package app.dao.springdata;

import org.springframework.data.repository.CrudRepository;

import app.domain.Account;

public interface AccountRepository  extends CrudRepository<Account, Integer>{
	public Account findByAccNo(String accNo);
}
