package app.domain;

public enum HolderType {
	
	OWNER, REPRESENTATIVE, ATTORNEY;
	
	public String getHolderType(){
		return this.name();
	}
}
