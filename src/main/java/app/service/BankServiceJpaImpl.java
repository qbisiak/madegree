package app.service;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import app.domain.Account;
import app.domain.Customer;
import app.domain.Participant;
import app.domain.Status;
import app.domain.Tariff;

@Repository
@Profile("jpa")
public class BankServiceJpaImpl implements BankService {

	private static final String SELECT_ACCOUNT_BY_EXT_ID =
			"select a from Account as a where accNo = :accNo";
	
	private EntityManager entityManager;

	@Autowired
	public BankServiceJpaImpl(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public void moneyTransfer(String accNo, BigDecimal amount) {
		
		Account account = entityManager
				.createQuery(SELECT_ACCOUNT_BY_EXT_ID, Account.class)
				.setParameter("accNo", accNo)
				.getSingleResult();
		
		if (account != null) {
			account.setBalance(account.getBalance().add(amount));
			account.setAvailableBalance(
					account.getAvailableBalance().add(amount));
			entityManager.merge(account);
		}
	}

	@Override
	@Transactional
	public void openAccount(Participant participant) {
		Customer customer = entityManager
				.createQuery("select c from Customer as c where extId = :extId", Customer.class)
				.setParameter("extId", participant.getCustomer().getExtId()).getSingleResult();

		Tariff tariff = entityManager.createQuery("select t from Tariff as t where extId = :extId", Tariff.class)
				.setParameter("extId", participant.getAccount().getTariff().getExtId()).getSingleResult();

		participant.getAccount().setTariff(tariff);
		participant.getAccount().setStatus(Status.ACTIVE);
		participant.setCustomer(customer);

		entityManager.persist(participant.getAccount());
		entityManager.persist(participant);
	}

	@Override
	@Transactional
	public void addCustomer(Customer customer) {
		entityManager.persist(customer);

	}

	@Override
	@Transactional
	public void addCustomers(List<Customer> customers) {
		for (Customer customer : customers) {
			entityManager.persist(customer);
		}
	}

	// @Override
	// public void deleteAll() {
	// participantRepository.deleteAll();
	// accountRepository.deleteAll();
	// customerRepository.deleteAll();
	// }
}
