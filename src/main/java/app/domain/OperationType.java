package app.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "operation_type")
public class OperationType implements Serializable {

	private static final long serialVersionUID = -5005696842286525759L;
	private Integer id;
	private Tariff tariff;
	private String extId;
	private BigDecimal constCharge;
	private BigDecimal interest;
	private Set<Operation> operations = new HashSet<>();

	public OperationType() {
	}

	public OperationType(Integer id) {
		this.id = id;
	}

	public OperationType(Integer id, Tariff tariff, String extId, BigDecimal constCharge, BigDecimal interest, Set<Operation> operations) {
		this.id = id;
		this.tariff = tariff;
		this.extId = extId;
		this.constCharge = constCharge;
		this.interest = interest;
		this.operations = operations;
	}

	@Id
	@SequenceGenerator(name="operation_type_sequence", sequenceName="operation_type_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="operation_type_sequence")
	@Column(name="id", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tariff_id")
	public Tariff getTariff() {
		return this.tariff;
	}

	public void setTariff(Tariff tariff) {
		this.tariff = tariff;
	}

	@Column(name = "ext_id")
	public String getExtId() {
		return this.extId;
	}

	public void setExtId(String extId) {
		this.extId = extId;
	}

	@Column(name = "const_charge")
	public BigDecimal getConstCharge() {
		return this.constCharge;
	}

	public void setConstCharge(BigDecimal constCharge) {
		this.constCharge = constCharge;
	}

	@Column(name = "interest", precision = 3)
	public BigDecimal getInterest() {
		return this.interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "operationType")
	public Set<Operation> getOperations() {
		return this.operations;
	}

	public void setOperations(Set<Operation> operations) {
		this.operations = operations;
	}

}
