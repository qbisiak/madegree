package app.dao.jdbc;

public class JdbcDaoQuery {

	public final static String FIND_ACCOUNT_BY_ACCNO = "select id, acc_no, status, balance, available_balance, own_funds, transaction_per_day, amount_per_day, transaction_per_month, amount_per_month from account where acc_no=?";
	public final static String UPDATE_ACCOUNT_FUNDS = "update account available_balance=?, balance=?, own_funds=? where id=?";
	public final static String INSERT_ACCOUNT_RETURNING_ID = "insert into account (acc_no, rel_account_id, amount_per_day, amount_per_month, available_balance, balance, own_funds, status, transaction_per_day, transaction_per_month, tariff_id) select ?,?,?,?,?,?,?,?,?,?,id from tariff where ext_id=? returning id";
	public final static String INSERT_PARTICIPANT = "insert into participant (status, holder_type, customer_id, account_id) values (?,?,?,?)";
	public final static String FIND_CUSTOMER_BY_EXT_ID = "select id, ext_id, name, name2, surname, surname2, sex, status from customer where ext_id=?";
	public final static String INSERT_CUSTOMER = "insert into customer(ext_id, name, name2, surname, surname2, sex, status) values (?,?,?,?,?,?,?)";
	public final static String DELETE_ALL_CUSTOMERS = "delete from customer";
	public final static String DELETE_ALL_ACCOUNTS = "delete from account";
	public final static String DELETE_ALL_PARTICIPANTS = "delete from participant";
	public final static String GET_CUSTOMER_ID_BY_EXT_ID = "select id from customer where ext_id=?";
	
	private JdbcDaoQuery(){
		
	}
}
