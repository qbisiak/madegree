package app.service;

import java.math.BigDecimal;
import java.util.List;

import app.domain.Customer;
import app.domain.Participant;

public interface BankService {
	
	public void moneyTransfer(String accNo, BigDecimal amount);
	public void openAccount(Participant participant);
	public void addCustomer(Customer customer);
	public void addCustomers(List<Customer> customers);
//	public void deleteAll();
}
