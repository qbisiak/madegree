package app.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.dao.jdbc.JdbcPreparedStatementDao;
import app.dao.jdbc.JdbcTemplateDao;
import app.domain.Account;
import app.domain.Customer;
import app.domain.Participant;

@Service
@Profile("jdbcps")
public class BankServiceJdbcPreparedStatementImpl implements BankService{

	private JdbcTemplateDao jdbcDao;
	private JdbcPreparedStatementDao jdbcPsDao;
	
	@Autowired
	public BankServiceJdbcPreparedStatementImpl(JdbcTemplateDao jdbcDao, JdbcPreparedStatementDao jdbcPsDao) {
		super();
		this.jdbcDao = jdbcDao;
		this.jdbcPsDao = jdbcPsDao;
	}

	@Override
	@Transactional
	public void moneyTransfer(String accNo, BigDecimal amount) {
		Account account = jdbcDao.findByAccNo(accNo);
		if (account != null) {
			account.setBalance(account.getBalance().add(amount));
			account.setAvailableBalance(account.getAvailableBalance().add(amount));
			jdbcDao.updateFuds(account);
		}
	}

	@Override
	public void openAccount(Participant participant) {
		jdbcPsDao.openAccount(participant);
	}

	@Override
	public void addCustomer(Customer customer) {
		jdbcPsDao.insertCustomer(customer);
	}

	@Override
	public void addCustomers(List<Customer> customers) {
		jdbcPsDao.insertCustomers(customers);
		
	}

//	@Override
//	public void deleteAll() {
//		jdbcDao.deleteAll();
//	}
}
