package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MadegreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MadegreeApplication.class, args);
	}
}
