package app.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product implements Serializable {

	private static final long serialVersionUID = -4449399629565327209L;
	private Integer id;
	private String extId;
	private String name;
	private String type;
	private Set<Tariff> tariffs = new HashSet<>();

	public Product() {
	}

	public Product(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Product(Integer id, String extId, String name, String type, Set<Tariff> tariffs) {
		this.id = id;
		this.extId = extId;
		this.name = name;
		this.type = type;
		this.tariffs = tariffs;
	}

	@Id
	@SequenceGenerator(name="product_sequence", sequenceName="product_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="product_sequence")
	@Column(name="id", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ext_id")
	public String getExtId() {
		return this.extId;
	}

	public void setExtId(String extId) {
		this.extId = extId;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	public Set<Tariff> getTariffs() {
		return this.tariffs;
	}

	public void setTariffs(Set<Tariff> tariffs) {
		this.tariffs = tariffs;
	}

}
