package app.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages={"app.dao"})
@EntityScan(basePackages={"app.domain"})
public class AppConfig {

}
