package app.dao.jdbc;

import java.util.List;

import app.domain.Customer;
import app.domain.Participant;

public interface JdbcPreparedStatementDao {
	public void insertCustomer(Customer customer);
	public void insertCustomers(List<Customer> customers);
	public void openAccount(Participant participant);
}
