package app.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tariff")
public class Tariff implements Serializable {

	private static final long serialVersionUID = 3779222842922640855L;
	private Integer id;
	private Product product;
	private String extId;
	private BigDecimal interest;
	private BigDecimal monthlyFee;
	private BigDecimal annualFee;
	private Set<OperationType> operationTypes = new HashSet<>();
	private Set<Account> accounts = new HashSet<>();

	public Tariff() {
	}

	public Tariff(Integer id, Product product) {
		this.id = id;
		this.product = product;
	}

	public Tariff(Integer id, Product product, String extId, BigDecimal interest, BigDecimal monthlyFee, BigDecimal annualFee,
			Set<OperationType> operationTypes, Set<Account> accounts) {
		this.id = id;
		this.product = product;
		this.extId = extId;
		this.interest = interest;
		this.monthlyFee = monthlyFee;
		this.annualFee = annualFee;
		this.operationTypes = operationTypes;
		this.accounts = accounts;
	}

	@Id
	@SequenceGenerator(name="tariff_sequence", sequenceName="tariff_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tariff_sequence")
	@Column(name="id", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name = "ext_id")
	public String getExtId() {
		return this.extId;
	}

	public void setExtId(String extId) {
		this.extId = extId;
	}

	@Column(name = "interest", precision = 3)
	public BigDecimal getInterest() {
		return this.interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	@Column(name = "monthly_fee")
	public BigDecimal getMonthlyFee() {
		return this.monthlyFee;
	}

	public void setMonthlyFee(BigDecimal monthlyFee) {
		this.monthlyFee = monthlyFee;
	}

	@Column(name = "annual_fee")
	public BigDecimal getAnnualFee() {
		return this.annualFee;
	}

	public void setAnnualFee(BigDecimal annualFee) {
		this.annualFee = annualFee;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tariff")
	public Set<OperationType> getOperationTypes() {
		return this.operationTypes;
	}

	public void setOperationTypes(Set<OperationType> operationTypes) {
		this.operationTypes = operationTypes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tariff")
	public Set<Account> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

}
