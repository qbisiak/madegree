package app.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = -786391802602454087L;
	private Integer id;
	private String extId;
	private String name;
	private String name2;
	private String surname;
	private String surname2;
	private char sex;
	private Status status;
	private Set<Participant> participants = new HashSet<Participant>();

	public Customer() {
	}

	public Customer(Integer id, String name, String surname, char sex, Status status) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.sex = sex;
		this.status = status;
	}

	public Customer(Integer id, String extId, String name, String name2, String surname, String surname2, char sex,
			Status status, Set<Participant> participants) {
		this.id = id;
		this.extId = extId;
		this.name = name;
		this.name2 = name2;
		this.surname = surname;
		this.surname2 = surname2;
		this.sex = sex;
		this.status = status;
		this.participants = participants;
	}

	@Id
	@SequenceGenerator(name="customer_sequence", sequenceName="customer_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="customer_sequence")
	@Column(name="id", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ext_id")
	public String getExtId() {
		return this.extId;
	}

	public void setExtId(String extId) {
		this.extId = extId;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "name2")
	public String getName2() {
		return this.name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	@Column(name = "surname", nullable = false)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Column(name = "surname2")
	public String getSurname2() {
		return this.surname2;
	}

	public void setSurname2(String surname2) {
		this.surname2 = surname2;
	}

	@Column(name = "sex", nullable = false, length = 1)
	public char getSex() {
		return this.sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
	public Set<Participant> getParticipants() {
		return this.participants;
	}

	public void setParticipants(Set<Participant> participants) {
		this.participants = participants;
	}

}
